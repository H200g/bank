
terraform {
  backend "s3" {
    bucket= "cf-bucketyu"
    key    = "test/terraform.tfstate"
    region = "us-east-1"
    #dynamodb_table = "-tfstate-dynamoDB"
    encrypt = true
  }
}
